<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUser')->unsigned()->nullable();
            $table->integer('idReservation')->unsigned();
            $table->string('observations')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->integer('telephone')->length(9)->nullable();
            $table->integer('assistants')->length(2);
            $table->foreign('idUser')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('idReservation')->references('id')->on('reservations')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->boolean('waitingList')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_reservations');
    }
}
