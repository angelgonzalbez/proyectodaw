<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idTeacher')->unsigned();
            $table->integer('idReservation')->unsigned();
            $table->foreign('idTeacher')->references('id')->on('teachers')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('idReservation')->references('id')->on('reservations')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_teachers');
    }
}
