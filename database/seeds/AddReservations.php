<?php

use Illuminate\Database\Seeder;
use App\Reservation;

class AddReservations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reservation = new Reservation();
        $reservation->date = date('Y-m-d');
        $reservation->timetable = strftime('%X');
        $reservation->spaces = 5;
        $reservation->overbooking = 0;
        $reservation->waitingList = 0;
        $reservation->save();
    }
}
