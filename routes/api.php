<?php

use Illuminate\Http\Request;

/** Reset reservation **/
Route::delete('/reset-reservation/cancel','Api\ResetReservationController@delete');
Route::put('/reset-reservation/modify','Api\ResetReservationController@update');

/** Edit or cancel reservations by Admin **/
Route::group(['middleware' => 'auth:api'], function() {
    Route::get('/reservation/assistants/max/{id}','Api\ReservationController@getMaxAssistants');
    Route::get('/reservation/{id}', 'Api\ReservationController@get');
    Route::delete('/reservation/{id}','Api\ReservationController@delete');
    Route::put('/reservation/edit/{id}','Api\ReservationController@update');
});