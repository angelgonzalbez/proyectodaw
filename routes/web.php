<?php

/** Web routes **/

/** Auth routes **/
Auth::routes();

/** Routes without autentication **/
Route::get('/', 'SiteController@home');
Route::get('/about','SiteController@about');
Route::get('/home', 'HomeController@index')->name('home'); // Redirects to login
Route::get('/gallery', 'ImageController@getGallery');
Route::get('/booktable', 'ReservationController@index');
Route::post('/reservationForm', 'ReservationController@reservationForm');
Route::get('/reservationDone', 'ReservationController@getReservationForm');
Route::post('/reservationDone', 'ReservationController@reservationDone');
Route::get('/gallery/{year}', 'ImageController@getYearGallery');
Route::get('/gallery/{year}/{month}', 'ImageController@getMonthGallery');
Route::get('/gallery/{year}/{month}/{day}', 'ImageController@getDayGallery');

/** Reset reservation **/
Route::get('/reservation/reset/{token}','Reservation\ResetReservationController@get');
Route::get('/reservation/canceled','Reservation\ResetReservationController@canceled');
Route::get('/reservation/modified','Reservation\ResetReservationController@modified');

/** Routes when you are autenticated **/
Route::group(['middleware' => 'auth'], function() {
    Route::get('reservation/create', 'ReservationController@makeReservation')->middleware('role:admin');
    Route::post('reservation/create', 'ReservationController@saveReservation')->middleware('role:admin');
    Route::post('uploadGalleryImage', 'ImageController@uploadGalleryImage')->middleware('role:admin');
    Route::get('/administration', 'HomeController@getAdminPanel')->middleware('role:admin');
    Route::get('/administration/futureReservations', 'ReservationController@getFutureReservations')->middleware('role:admin');
    Route::get('/administration/pastReservations', 'ReservationController@getPastReservations')->middleware('role:admin');    
    Route::get('/user/reservations', 'HomeController@getUserReservations')->middleware('role:user:admin');
    Route::get('/logout', 'Auth\LoginController@logout')->middleware('role:admin:user');
    Route::get('/user', 'HomeController@getUserEditPanel')->middleware('role:admin:user');
    Route::put('/user', 'UserController@edit')->middleware('role:admin:user');
    Route::post('/createReservation', 'ReservationController@createReservation')->middleware('role:user:admin');
    Route::post('/createReservationForm', 'ReservationController@reservationForm')->middleware('role:user:admin');
    Route::get('/administration/createTeacher', 'HomeController@createTeacher')->middleware('role:admin');
    Route::post('/administration/createTeacherDone', 'HomeController@createTeacherDone')->middleware('role:admin');
    Route::get('/administration/teacherList', 'HomeController@teacherList')->middleware('role:admin');
    Route::get('/administration/teacher/delete/{id}', 'HomeController@deleteTeacher')->middleware('role:admin');
    Route::get('/administration/teacher/edit/{id}', 'HomeController@editTeacherForm')->middleware('role:admin');
    Route::post('/administration/editTeacherDone', 'HomeController@editTeacherDone')->middleware('role:admin');
    Route::get('/reservation/{id}/accept','ReservationController@accept')->middleware('role:admin');
});
