<?php

return [
	'catalogo' => ['url' => '/catalog'],
	'nueva' => ['title' => 'Nueva Película', 'url' => '/catalog/create'],
	'logout' => ['title' => 'Cerrar Sesion' ,'url' => '/logout']
];