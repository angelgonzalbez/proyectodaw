<?php

return [
	'spaces' => env('SPACES_DEFAULT', 15),
	'overbooking' => env('OVERBOOKING_DEFAULT', 5),
	'waitingList' => env('WAITING_LIST_DEFAULT', 0)
];