@extends('layouts.master')
@section('content')
<!--================ Start banner Area =================-->
<section class="error-banner-404 relative">
    <div class="container">
        <div class="row height align-items-center justify-content-center">
            <div class="home-banner-content col-lg-5">
                <h1>L'ASSAIG</h1>
                <hr>
                <h2 class="h2 white-bg">ERROR DE CONEXIÓN CON LA BAS DE DATOS</h2>
            </div>
        </div>
    </div>
</section>
<!--================ End banner Area =================-->
<br>
@stop