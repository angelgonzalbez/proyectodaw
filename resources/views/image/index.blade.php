@extends('layouts.master')
@section('content')
<!--================ Start banner Area =================-->
<section class="home-banner-area relative">
    <div class="container">
        <div class="row height align-items-center justify-content-center">
            <div class="home-banner-content col-lg-5">
                <h1>L'ASSAIG</h1>
                <hr>
                <p>A Fine Dinning Restaurant</p>
            </div>
        </div>
    </div>
</section>
<!--================ End banner Area =================-->

<!--================ Left Right And Banner Icon =================-->
<div class="fixed-view-menu">
    <p>
        <a href="menu.html">view menu</a>
    </p>
</div>
<div class="fixed-book-table">
    <p>
        <a href="index.html">book a table</a>
    </p>
</div>
<br>
<!--================ Left Right And Banner Icon =================-->
@stop