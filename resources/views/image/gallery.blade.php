@extends('layouts.master')
@section('content')
<!--================ Start banner Area =================-->
<section class="banner-area relative">
    <div class="gallery-container">
        <div class="row height align-items-center justify-content-center">
            <div class="banner-content col-lg-6">
                <h1>galería de imágenes</h1>
                <hr>
                <div class="breadcrmb">
                    <p>
                        <a href="/">inicio</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="/gallery">galería</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ End banner Area =================-->
<!--================ Gallery Area =================-->
<section class="gallery-area section-gap mt-172">

    <div class="gallery-container">
        <div class="row">
            <div class="col-12 col-lg-9">
                <div class="row">
                    @foreach( $imagesArray as $image )
                    <div class="col-lg-4 col-md-6">
                        <div class="single-gallery mt--350">
                            <div class="overlay"></div>
                            <figure>
                                <img src="/img/gallery/{{$image->name}}" class="img-fluid" alt="">
                            </figure>
                            <div class="icon">
                                <a href="/img/gallery/{{$image->name}}" class="photo-gallery-pop-up">
                                    <span class="lnr lnr-cross"></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    @endforeach 
                </div>
            </div>


            <div class='col-12 col-md-6 offset-md-6 offset-lg-0 col-lg-3'>
                <div class='widget BlogArchive' data-version='1' id='BlogArchive1'>
                    <h2>Imágenes</h2>
                    <div class='widget-content'>
                        <div id='ArchiveList'>
                            <div id='BlogArchive1_ArchiveList'>
                                @foreach($fechas as $anyo => $meses)
                                <ul class='hierarchy'>
                                    <li class='archivedate collapsed'>
                                        <a class='toggle' href='javascript:void(0)'>
                                            <span class='zippy'>

                                                &#9658;&#160;

                                            </span>
                                        </a>
                                        <a class='post-count-link' href='<?php echo '/gallery/'.$anyo ?>'>
                                            <?php echo $anyo ?>
                                        </a>
                                        <span class='post-count' dir='ltr'>(<?php echo count($meses) ?>)</span>
                                        @foreach($meses as $mes => $dias)
                                        <ul class='hierarchy'>
                                            <li class='archivedate collapsed'>
                                                <a class='toggle' href='javascript:void(0)'>
                                                    <span class='zippy'>

                                                        &#9658;&#160;

                                                    </span>
                                                </a>
                                                <a class='post-count-link' href='<?php echo '/gallery/'.$anyo.'/'.$mes ?>'>
                                                    <?php echo $mesesString[$mes - 1] ?>
                                                </a>
                                                <span class='post-count' dir='ltr'>(<?php echo count($dias) ?>)</span>
                                                @foreach($dias as $dia)
                                                <ul class='posts'>
                                                    <li><a href='<?php echo '/gallery/'.$anyo.'/'.$mes.'/'.$dia ?>'><?php echo 'Menú '.$dia.' '.$mesesString[$mes - 1].' '.$anyo ?></a></li>
                                                </ul>
                                                @endforeach
                                            </li>
                                        </ul>
                                        @endforeach
                                    </li>
                                </ul>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ $imagesArray->links() }}
    <br>
    @if(Auth::user())
    @if (Auth::user()->hasRole('Admin'))
    <form class="push-image" action="/uploadGalleryImage" method="post" enctype="multipart/form-data">
        @csrf
        Subir imagen: <input type="file" name="file" id="file"> <input class="btn btn-primary" type="submit" value="Subir foto">
    </form>
    @endif
    @endif
</section>
<!--================ End Gallery Area =================-->
@stop