@extends('layouts.master')
@section('content')
<!--================ Start banner Area =================-->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <!-- Slide One -->
        <div class="carousel-item active" style="background-image: url('img/slide1.png')">
            <div class="carousel-caption d-none d-md-block">
                <h3></h3>
                <p></p>
            </div>
        </div>
        <!-- Slide Two -->
        <div class="carousel-item" style="background-image: url('img/slide2.jpg')">
            <div class="carousel-caption d-none d-md-block">
                <h3></h3>
                <p></p>
            </div>
        </div>
        <!-- Slide Three -->
        <div class="carousel-item" style="background-image: url('img/slide3.jpg')">
            <div class="carousel-caption d-none d-md-block">
                <h3></h3>
                <p></p>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!--================ End banner Area =================-->

<!--================ Left Right And Banner Icon =================-->
<div class="fixed-view-menu">
    <p>
        <a href="/gallery">galería</a>
    </p>
</div>
<div class="fixed-book-table">
    <p>
        <a href="/booktable">Reservar</a>
    </p>
</div>
<br>
<div class="container">

    <h1 class="my-4"></h1>

    <!-- Marketing Icons Section -->
    <div class="row">
      <div class="col-lg-4 mt-4mb-4">
        <div class="card h-100">
          <h4 class="card-header">Ven a disfrutar con tus amigos y familiares</h4>
          <div class="card-body">
            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus
              neque.</p>
          </div>

        </div>
      </div>
      <div class="col-lg-4 mb-4">
        <div class="card h-100">
          <h4 class="card-header">Querrás repetir</h4>
          <div class="card-body">
            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ipsam eos, nam
              perspiciatis natus commodi similique totam consectetur praesentium molestiae atque exercitationem ut
              consequuntur, sed eveniet, magni nostrum sint fuga.</p>
          </div>

        </div>
      </div>
      <div class="col-lg-4 mb-4">
        <div class="card h-100">
          <h4 class="card-header">Trato familiar</h4>
          <div class="card-body">
            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus
              neque.</p>
          </div>

        </div>
      </div>
    </div>
<!--================ Left Right And Banner Icon =================-->
@stop