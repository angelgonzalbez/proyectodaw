@extends('layouts.master')
@section('content')

<!--================ Start banner Area =================-->
<section class="banner-area relative">
    <div class="container">
        <div class="row height align-items-center justify-content-center">
            <div class="banner-content col-lg-6">
                <h1>sobre nosotros</h1>
                <hr>
                <div class="breadcrmb">
                    <p>
                        <a href="/">inicio</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="/about">conócenos</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ End banner Area =================-->

<!--================ About Area =================-->
<section class="about-area section-gap-top">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-6">
                <div class="">
                    <img class="img-fluid" src="img/about-img.jpg" alt="">
                </div>
            </div>
            <div class="offset-lg-1 col-lg-5 col-md-6">
                <div class="section-title relative">
                    <h1>
                        Sobre <br>
                        L'assaig <br>
                        Batoi
                    </h1>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                        enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ End About Area =================-->

<!--================ Start Call To Action Area =================-->
<section class="container section-gap-top">
    <div class="callto-action-area relative">
        <div class="row d-flex justify-content-center">
            <div class="col-lg-12 p-0">
                <div class="cta-owl owl-carousel">
                    <div class="item">
                        <div class="cta-img">
                            <img src="img/callaction-bg.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="text-box text-center">
                            <h3 class="mb-10">Main Course</h3>
                            <p>
                                Chicken Steak with gerlic bread & Fries
                            </p>
                        </div>
                    </div>               
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ End Call To Action Area =================-->

<!--================ Chefs Quotes Area =================-->
<section class="chefs-quotes-area section-gap">
    <div class="container">
<!--        <div class="row align-items-center">
            <div class="col-lg-5">
                <div class="section-title relative">
                    <h1>
                        Notas del <br>
                        Chef
                    </h1>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                        enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Quotes
                    </p>
                    <img src="img/signature.png" class="img-fluid" alt="">
                </div>
            </div>
            <div class="offset-lg-1 col-lg-6 col-md-6">
                <div>
                    <img class="chef-img" src="img/chefs-quotes.jpg" class="img-fluid" alt="">
                </div>
            </div>
        </div>-->
        <div class="row align-items-center">
            <div class="col-12 col-lg-6 col-md-6 mt-30">
                <map id="contactMap">
                <h1>
                    Dónde estamos?
                </h1>
                    <iframe class="mapa-responsive" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10474.555902196229!2d-0.4899317030075596!3d38.69245295191906!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd618702fd4eb5b1%3A0xab5dffe40dc99b43!2sCIP+de+FP+Batoi!5e0!3m2!1ses!2ses!4v1549552394481" width="500" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
            </map>
            </div>
            
            <div class="col-lg-6 col-md-6"
                 <p>
                    Teléfono: 675483921
                </p>
                <p>
                    Dirección: Calle Falsa, N123
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
            </div>
        </div>

    </div>
</section>
<!--================ End Chefs Quotes Area =================-->

@stop