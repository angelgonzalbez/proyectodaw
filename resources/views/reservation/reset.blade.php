@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                <form class="auth-forms">
                    @csrf
                    <input id="token" name="token" type="hidden" value="{{$token}}">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $nombre }}" required autofocus>
                            @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="telephone" class="col-md-4 col-form-label text-md-right">{{ __('Telephone') }}</label>
                        <div class="col-md-6">
                            <input min="9" max="9" id="telephone" type="text" class="form-control{{ $errors->has('telephone') ? ' is-invalid' : '' }}" name="telephone" value="{{ $telefono }}" required>
                            @if ($errors->has('telephone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('telephone') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="assistants" class="col-md-4 col-form-label text-md-right">{{ __('Assistants') }}</label>
                        <div class="col-md-6">
                            <input id="assistants" type="number" class="form-control{{ $errors->has('assistants') ? ' is-invalid' : '' }}" name="assistants" min="1" max="{{$maxAssistants}}" value="{{ $persons }}">
                            @if ($errors->has('assistants'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('assistants') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="observations" class="col-md-4 col-form-label text-md-right">{{ __('Observations') }}</label>
                        <div class="col-md-6">
                            <input id="observations" type="text" class="form-control{{ $errors->has('observations') ? ' is-invalid' : '' }}" name="observations" placeholder="{{ __('Allergies and intolerances') }}" value="{{ $comment }}">
                            @if ($errors->has('observations'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('observations') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-2 offset-md-4">
                            <button id="save" type="submit" class="btn btn-primary">
                                {{ __('Guardar') }}
                            </button>
                        </div>
                        <div class="col-md-4 ">
                            <button id="cancel" type="submit" class="btn btn-danger">
                                {{ __('Cancelar reserva') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br>
<!-- MODAL (TEXT) -->
<div class="modal fade" id="alertReservationModal" tabindex="-1" role="dialog" aria-labelledby="reservation" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Aviso</h5>
      </div>
        <div id="alertModal" class="modal-body">
      </div>
        <div id="alertModalFooter" class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@stop