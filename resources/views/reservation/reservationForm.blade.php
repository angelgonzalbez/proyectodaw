@extends('layouts.masterDaniel')

@section('content')
<main style="background-image: url(img/imgBg3.jpg);    background-position: center;    background-size: cover;">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                <form class="auth-forms formTransparente" method="POST" action="/reservationDone">
                    @csrf

                    <input id="reservationId" name="reservationId" type="hidden" value="{{$reservationId}}">
                    <input id="onWaitingList" name="onWaitingList" type="hidden" value="{{$onWaitingList}}">

                    @if(Session::has('flash_message'))
                        <div class="alert alert-warning text-center"><span class="fa fa-warning" aria-hidden="true"> {!! session('flash_message') !!}</span></div>
                    @endif

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $nombre }}" required autofocus>

                            @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email }}">

                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="telephone" class="col-md-4 col-form-label text-md-right">{{ __('Telephone') }}</label>

                        <div class="col-md-6">
                            <input id="telephone" type="text" class="form-control{{ $errors->has('telephone') ? ' is-invalid' : '' }}" name="telephone" value="{{ $telefono }}" required>

                            @if ($errors->has('telephone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('telephone') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="assistants" class="col-md-4 col-form-label text-md-right">{{ __('Assistants') }}</label>

                        <div class="col-md-6">
                            <input id="assistants" type="number" class="form-control{{ $errors->has('assistants') ? ' is-invalid' : '' }}" name="assistants" min="0" max="{{$maxAssistants}}" required>

                            @if ($errors->has('assistants'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('assistants') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="observations" class="col-md-4 col-form-label text-md-right">{{ __('Observations') }}</label>

                        <div class="col-md-6">
                            <textarea id="assistants" class="form-control{{ $errors->has('observations') ? ' is-invalid' : '' }}" name="observations" placeholder="{{ __('Allergies and intolerances') }}"></textarea>

                            @if ($errors->has('assistants'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('assistants') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Reserve') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</main>
<br>
@endsection
