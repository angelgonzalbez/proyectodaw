<script>
    $(document).ready(function(){
        $('#calendar-{{ $calendar->getId() }}').fullCalendar({
            dayClick: function(date, jsEvent, view) {

            //alert('Clicked on: ' + date.format());
            
            /*reservations.forEach(function(element) {
                if(date.format() == '2019-02-19'){
                window.location.href = "/testing"+date.format();
                }
            });*/
            @foreach ($reservations as $reservation) 

            if(date.format() == '{{$reservation->getStartDate()}}' && '{{$reservation->getBackgroundColor()}}' != 'red'){
                    //window.location.href = "/reservationForm?date="+date.format()+"&reservationId="+{{$reservation->getReservationId()}};
                    $.redirect("/reservationForm", {_token: '{{ csrf_token() }}', date: date.format(), reservationId: "{{$reservation->getReservationId()}}", onWaitingList: {{$reservation->onWaitingList()}}}, "POST"); 
                    }
            @endforeach

                
            // change the day's background color just for fun
            $(this).css('background-color', 'green');

            },
            events: [
            
                @foreach ($reservations as $reservation) 
                {
                    start: '{{$reservation->getStartDate()}}',
                    end: '{{$reservation->getEndDate()}}',
                    backgroundColor: '{{$reservation->getBackgroundColor()}}',
                    rendering: 'background'
                },
                @if ($reservation->checkSpaces())
                {
                    start: '{{$reservation->getStartDate()}}',
                    end: '{{$reservation->getEndDate()}}',
                    title : '{{$reservation->getSpaces()}}'
                },
                @endif
                @endforeach
        
        ],weekends: false
        });

    });
</script>