@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h1>Reservas</h1>
        </div>
        @if(!$switch)
        <div class="row m-5">
            <a href="/administration/pastReservations">
                <button class="btn btn-primary">Historial de reservas</button>
            </a>
        </div>
        @else
        <div class="row m-5">
            <a href="/administration/futureReservations">
                <button class="btn btn-primary">Reservas pendientes</button>
            </a>
        </div>
        @endif
    </div>
    <table id="myTable" class="table table-nonfluid table-bordered">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Email</th>
                <th>Teléfono</th>
                <th>Personas</th>
                <th>Fecha</th>
                <th>Comentarios</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach( $reservations as $user_reservation )
            <tr id="tr{{$user_reservation->id}}">
                <td>{{$user_reservation->name}}</td>
                <td>{{$user_reservation->email}}</td>
                <td>{{$user_reservation->telephone}}</td>
                <td>{{$user_reservation->assistants}}</td>
                <td>{{$user_reservation->reservation->date}}</td>
                <td>{{$user_reservation->observations}}</td>
                <td>
                    @if(fecha_mayor($user_reservation->reservation->date))
                    <button id="{{$user_reservation->id}}" class="btn btn-warning" style= "display:inline" >Editar</button>
                    <button id="{{$user_reservation->id}}" class="btn btn-danger" style= "display:inline" >Cancelar</button>
                    @if($user_reservation->waitingList)
                    <a href="/reservation/{{$user_reservation->id}}/accept">
                    <button type="submit" class="btn btn-primary" style= "display:inline" >Pasar a reserva</button>
                    @endif
                    @else
                        No hay acciones disponibles
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <!-- MODAL (EDIT)-->
<div class="modal fade" id="reservationModal" tabindex="-1" role="dialog" aria-labelledby="reservation" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Reserva</h5>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      </div>
        <div id="modalFooter" class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
    <!-- RETURN -->
    <div class="row m-5">
        <a href="/administration">
            <button class="btn btn-primary">Volver</button>
        </a>
    </div>
</div>
@stop