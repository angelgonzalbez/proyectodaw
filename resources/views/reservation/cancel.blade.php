@extends('layouts.master')
@section('content')
<div class="container justify-content-center text-center">
    <div class="row">
    <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Reserva</h5>
                    <p class="card-text">Reserva cancelada</p>
                    <a href="/" class="btn btn-primary">Volver al inicio</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop