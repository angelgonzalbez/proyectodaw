@extends('layouts.master')

@section('content')
<div class="container">

<div class="card text-center">
  <div class="card-header">
    Reserva realizada satisfactoriamente
  </div>
  <div class="card-body">
    <h5 class="card-title">Información sobre la reserva</h5>
    <p class="card-text">Nombre: {{ $name }}</p>
    <p class="card-text">Numero de asistentes: {{ $assistants }} </p>
    <p class="card-text">Fecha: {{ $date }}</p>
    <p class="card-text">Hora: {{ $timetable }}</p>

    <a href="/" class="btn btn-primary">Volver al inicio</a>
  </div>
</div>
</div>
@endsection
