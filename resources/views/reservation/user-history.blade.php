@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row pt-100">
        <div class="col-12">
            <h1>Reservas del día</h1>
        </div>
    </div>
    <table id="myTable" class="table ttable-nonfluid table-bordered">
        <thead>
            <tr>
                <th>Personas</th>
                <th>Fecha</th>
                <th>Comentarios</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            {{ $bool = false }}
            @foreach( $reservations as $user_reservation )
            @if(Auth::user()->name==$user_reservation->name)
                                                           <?php $bool = true ?>
        <tr id="tr{{$user_reservation->id}}">
                <td>{{$user_reservation->assistants}}</td>
                <td>{{date_time_to_string($user_reservation->reservation->date,$user_reservation->reservation->timetable)}}</td>
                <td>{{$user_reservation->observations}}</td>
                <td>
                    @if(fecha_mayor($user_reservation->reservation->date))
                    <button id="{{$user_reservation->id}}" class="btn btn-warning" style= "display:inline" >Editar</button>
                    <button id="c{{$user_reservation->id}}" class="btn btn-danger" style= "display:inline" >Cancelar</button>
                    @else
                    No hay acciones disponibles
                    @endif
                </td>
            </tr>
            @else
            @endif
            @endforeach

        </tbody>
    </table>

    @if($bool == false)
        @if(Session::has('flash_message'))
        <div class="alert alert-warning text-center"><span class="fa fa-info" aria-hidden="true"> {!! session('flash_message') !!}</span></div>
        @endif
    @endif

<!-- MODAL (EDIT)-->
<div class="modal fade" id="reservationModal" tabindex="-1" role="dialog" aria-labelledby="reservation" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Reservation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
      </div>
    </div>
  </div>
</div>
    <!-- MODAL (DELETE) -->
<div class="modal fade" id="reservationDeleteModal" tabindex="-1" role="dialog" aria-labelledby="reservation" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cancelar reserva</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <p>¿Estas seguro de cancelar la reserva?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-danger">Cancelar</button>
      </div>
    </div>
  </div>
</div>
        <!-- MODAL (TEXT) -->
<div class="modal fade" id="alertReservationModal" tabindex="-1" role="dialog" aria-labelledby="reservation" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Aviso</h5>
          <span aria-hidden="true">&times;</span>
      </div>
        <div id="alertModal" class="modal-body">
      </div>
        <div id="alertModalFooter" class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
    <!-- RETURN -->
    <div class="row m-5">
        <a href="/administration">
            <button class="btn btn-primary">Volver</button>
        </a>
    </div>
</div>
@stop