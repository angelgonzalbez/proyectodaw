@extends('layouts.master')

@section('content')
<div class="container">
    <div class="card text-center">
        <div class="card-header">
            <h2>Aviso</h2>
        </div>
        <div class="card-body">
            <p>{{$message}}</p>
            <a href="/" class="btn btn-primary">Volver al inicio</a>
        </div>
    </div>
</div>
@endsection