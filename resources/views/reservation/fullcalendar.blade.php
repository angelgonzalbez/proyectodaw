@extends('layouts.calendar')

@section('content')
<div class="container">
    <div class="row">
    <div>
    <table class="leyenda">
    <tr><td> Disponible  </td><td> <img class="imgLeyenda" src="img/cuadradoVerde.jpg" alt="cuadradoVerde"> </td></tr>
    <tr><td> No disponible  </td><td> <img class="imgLeyenda" src="img/cuadradoRojo.jpg" alt="cuadradoRojo"> </td></tr>
    <tr><td> Lista de espera &nbsp </td><td> <img class="imgLeyenda" src="img/cuadradoAmarillo.jpg" alt="cuadradoAmarillo"> </td></tr>
    <tr><td> Plazas libres </td><td style="color: white;text-align: center;font-weight: bold;"> 5 </td></tr>
    </table>
    </div>
        <div class="col-md-10 mt-50 mb-50" style="margin-left: 15px;">
            <div class="panel panel-default">
<div class="panel-body">
                    {!! $calendar->calendar() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection