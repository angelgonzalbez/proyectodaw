@extends('layouts.master')
@section('content')
<section class="reservation-area">
    <div class="container">
        <div class="row align-items-center">
            <div>
                <form class="booking-form" method="POST" action="/reservationCreated">
                @csrf
                    <div class="row">
                        <label for="event_date" class="col-lg-6">Fecha:</label>
                        <label for="hora" class="col-lg-6 d-lg-block d-none">Hora:</label>
                        <div class="input-group col-lg-6 mb-20">
                            <input name="date" id="event_date" class="form-control" placeholder="Fecha" class="form-control" required="" type="date">
                            <span class="input-group-append">
                                <button class="btn btn-outline-secondary border-left-0 border-0" type="button">
                                    <i class="fa fa-calendar reservation-calendar"></i>
                                </button>
                            </span>
                        </div>
                        <label for="hora" class="col-12 d-lg-none">Hora:</label>
                        <div class="input-group col-lg-6 mb-20">
                            <input name="timetable" placeholder="Hora" value="14:00" class="form-control" required="" type="time">
                            <span class="input-group-append">
                                <button class="btn btn-outline-secondary border-left-0 border-0" type="button">
                                    <i class="fa fa-clock-o"></i>
                                </button>
                            </span>
                        </div>
                        <label for="plazas" class="col-lg-4">Plazas disponibles:</label>
                        <label for="overbooking" class="col-lg-4 d-none d-lg-block">Overbooking:</label>
                        <label for="lista-espera" class="col-lg-4 d-none d-lg-block">Lista de espera:</label>
                        <div class="input-group col-lg-4 mb-20">
                            <input name="spaces" placeholder="Plazas disponibles" value="{{ $spaces }}" class="form-control" required="" type="number">
                            <span class="input-group-append">
                                <button class="btn btn-outline-secondary border-left-0 border-0" type="button">
                                    <i class="fa fa-users"></i>
                                </button>
                            </span>
                        </div>
                        <label for="overbooking" class="col-12 d-lg-none">Overbooking:</label>
                        <div class="input-group col-lg-4 mb-20">
                            <input name="overbooking" placeholder="overbooking" value="{{ $overbooking }}" class="form-control" required="" type="number">
                            <span class="input-group-append">
                                <button class="btn btn-outline-secondary border-left-0 border-0" type="button">
                                    <i class="fa fa-user-plus"></i>
                                </button>
                            </span>
                        </div>
                        <label for="lista-espera" class="col-12 d-lg-none">Lista de espera:</label>
                        <div class="input-group col-lg-4 mb-20">
                            <input name="waitingList" placeholder="Lista de espera" value="{{ $waitingList }}" class="form-control" required="" type="number">
                            <span class="input-group-append">
                                <button class="btn btn-outline-secondary border-left-0 border-0" type="button">
                                    <i class="fa fa-pause"></i>
                                </button>
                            </span>
                        </div>
                        <label for="profesores-sala" class="col-lg-6">Profesores de sala:</label>
                        <label for="profesores-cocina" class="col-lg-6 d-none d-lg-block">Profesores de cocina:</label>
                        <div class="input-group col-lg-6 mb-20">
                            <select multiple name="profesores-sala[]" class="form-control selectpicker">
                                @foreach($profesores as $profesor)
                                    @if($profesor->department == 'sala')
                                    <option>{{$profesor->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <span class="input-group-append">
                                <button class="btn btn-outline-secondary border-left-0 border-0" type="button">
                                    <i class="fa fa-graduation-cap"></i>
                                </button>
                            </span>
                        </div>
                        
                        <label for="profesores-cocina" class="col-12 d-lg-none">Profesores de cocina:</label>
                        <div class="input-group col-lg-6 mb-20">
                            <select class="selectpicker form-control" name="profesores-cocina[]" multiple>
                                @foreach($profesores as $profesor)
                                    @if($profesor->department == 'cocina')
                                    <option>{{$profesor->name}}</option>
                                    @endif
                                @endforeach
                            </select>

                            <span class="input-group-append">
                                <button class="btn btn-outline-secondary border-left-0 border-0" type="button">
                                    <i class="fa fa-graduation-cap"></i>
                                </button>
                            </span>
                        </div>
                        <div class="col-lg-12 d-flex justify-content-end">
                            <button type="submit" class="primary-btn dark mt-30 text-uppercase">Crear Reserva</button>
                        </div>
                        <div class="alert-msg"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@stop