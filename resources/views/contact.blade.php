@extends('layouts.master')
@section('content')
<!--================ Start banner Area =================-->
<section class="banner-area relative">
    <div class="container">
        <div class="row height align-items-center justify-content-center">
            <div class="banner-content col-lg-6">
                <h1>contact us</h1>
                <hr>
                <div class="breadcrmb">
                    <p>
                        <a href="index.html">home</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="contact.html">contact</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ End banner Area =================-->
<!--================ Contact Area =================-->
<section class="contact-area section-gap">
    <div class="container">
        <div class="row align-items-start">
            <div class="col-lg-6 col-md-6">
                <div id="contactMap"></div>
            </div>
            <div class="offset-lg-1 col-lg-5 col-md-6">
                <div class="section-title relative">
                    <h1>
                        L'Assaig <br>
                        Fine Dinning <br>
                        Restaurant <br>
                    </h1>
                    <div class="mb-20">
                        <p>C/. Societat Unió Musical, 8</p>
                        <p>03802 Alcoi (Alacant)</p>
                    </div>
                    <div class="mb-20">
                        <p>966 52 76 60</p>
                        <p>966 52 76 60</p>
                        <p>966 52 76 60</p>
                    </div>
                    <div class="mail">
                        <p>lassaig@gmail.com</p>
                        <p>lassaig@hotmail.com</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ End Contact Area =================-->
@stop