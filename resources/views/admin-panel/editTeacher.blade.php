@extends('layouts.master')

@section('content')
<main style="background-image: url(img/imgBg3.jpg);    background-position: center;    background-size: cover;">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                <form class="auth-forms" method="POST" action="/administration/editTeacherDone">
                    @csrf
                    <input id="id" type="hidden" name="id" value="{{$id}}">

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{$name}}" required autofocus>
                            
                            @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        
                        <label for="departamento" class="col-md-4 col-form-label text-md-right">{{ __('Departamento') }}</label>
                        <div class="col-md-6">
                            <select class="form-control" name="department">
                        @if ($department == "sala")
                        <option value="sala" selected="selected">sala</option>
                        <option value="cocina">cocina</option>
                        @else
                        <option value="sala">sala</option>
                        <option value="cocina" selected="selected">cocina</option>
                        @endif
                        </select>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Editar profesor') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</main>
<br>
@endsection
