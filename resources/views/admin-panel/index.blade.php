@extends('layouts.master')
@section('content')
<div class="container-fluid text-center">
    <div class="row">
        <div class="col-12">
            <h1>Panel de administración</h1>
        </div>
        <div class="col-6 mb-20 text-center">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Reservas</h5>
                    <p class="card-text">Acceder a todas reservas</p>
                    <a href="/administration/futureReservations" class="btn btn-primary mt-10">Próximas</a>
                    <a href="/administration/pastReservations" class="btn btn-primary mt-10">Historial</a>
                </div>
            </div>
        </div>
        <div class="col-6 mb-20 text-center">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Profesores</h5>
                    <p class="card-text">Accede a todos los profesores</p>
                    <a href="/administration/createTeacher" class="btn btn-primary mt-10">Añadir</a>
                    <a href="/administration/teacherList" class="btn btn-primary mt-10">Listado</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop