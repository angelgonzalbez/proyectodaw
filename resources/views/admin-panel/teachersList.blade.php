@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <table id="myTable" class="table table-nonfluid table-bordered">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Departamento</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach( $teachersArray as $teacher )
            <tr id="tr{{$teacher->id}}">
                <td>{{$teacher->name}}</td>
                <td>{{$teacher->department}}</td>
                <td>
                <a href="teacher/edit/{{$teacher->id}}"><button id="{{$teacher->id}}" class="btn btn-warning" style= "display:inline" >Editar</button></a>
                <a href="teacher/delete/{{$teacher->id}}"><button id="{{$teacher->id}}" class="btn btn-danger" style= "display:inline" >Eliminar</button></a>
                </td>

            </tr>
            @endforeach
        </tbody>
    </table>
    <!-- MODAL (EDIT)-->
<div class="modal fade" id="reservationModal" tabindex="-1" role="dialog" aria-labelledby="reservation" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Reserva</h5>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      </div>
        <div id="modalFooter" class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
    <!-- RETURN -->
    <div class="row m-5">
        <a href="/administration">
            <button class="btn btn-primary">Volver</button>
        </a>
    </div>
</div>
@stop