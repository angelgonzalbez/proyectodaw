<!--================ Start Footer Area =================-->
<footer class="footer-area section-gap">
    <div class="container">
        <div class="footer-bottom row align-items-center justify-content-between">
            <p class="footer-text m-0 col-lg-6 col-md-12">Copyright © 2018 All rights reserved | This template is made with
                <span class="lnr lnr-heart"></span> by <a href="#">Colorlib</a></p>
                <div class="col-lg-6 col-sm-12 footer-social">
                    <a title="Facebook" href="https://www.facebook.com/cfpbatoi"><i class="fa fa-facebook"></i></a>
                <a title="Blog" href=" https://cuinaalcoi.blogspot.com/?m=1"><i class="fa fa-newspaper-o"></i></a>
                <a title="Instagram" href="https://instagram.com/hosteleria_cipfp_batoi?utm_source=ig_profile_share&igshid=1meuph5lsg6bf"><i class="fa fa-instagram"></i></a>
            </div>
        </div>
    </div>
</footer>
<!--================ End Footer Area =================-->