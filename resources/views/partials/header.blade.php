<!--================ Start Header Area =================-->
<header class="header-area">
    <div class="container">
        <div class="header-wrap">
            <div class="header-top d-flex justify-content-between align-items-center navbar-expand-md">
                <div class="col menu-left">
                    <ul class="navbar-nav w-100">
                        <li class="nav-item mr-30" ><a class="<?php echo(checkActiveMenuOption('/') ? 'active' : '') ?>" href="/">inicio</a></li>
                        <li class="nav-item mr-30" ><a class="<?php echo(checkActiveMenuOption('/about') ? 'active' : '') ?>" href="/about">conócenos</a></li>
                        @if(Auth::user())
                        @if (Auth::user()->hasRole('Admin'))
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="#" data-toggle="dropdown"> Reservas</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item <?php echo(checkActiveMenuOption('/booktable') ? 'active text-dark' : '') ?>" href="/booktable">reservar</a>
                                <a class="dropdown-item <?php echo(checkActiveMenuOption('/reservation/create') ? 'active text-dark' : '') ?>" href="/reservation/create">crear reserva</a>
                            </div>
                        </li>
                        @else 
                        <li class="nav-item" ><a class="<?php echo(checkActiveMenuOption('/booktable') ? 'active' : '') ?>" href="/booktable">reservar</a></li>
                        @endif
                        @endif

                        @guest
                        <li class="nav-item" ><a class="<?php echo(checkActiveMenuOption('/booktable') ? 'active' : '') ?>" href="/booktable">reservar</a></li>
                        @endguest
                    </ul>
                </div>
                <div class="col-3 logo">
                    <a href="/"><img class="mx-auto" src="/img/logo.jpg" alt=""></a>
                </div>
                <nav class="col navbar navbar-expand-md justify-content-end" style="margin-bottom: 0;">
                    <!-- Toggler/collapsibe Button -->
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                        <span class="lnr lnr-menu"></span>
                    </button>
                    <!-- Navbar links -->
                    <div class="collapse navbar-collapse menu-right" id="collapsibleNavbar">
                        <ul class="navbar-nav justify-content-center w-100">
                            <li class="nav-item hide-lg">
                                <a class="nav-link <?php echo(checkActiveMenuOption('/') ? 'active' : '') ?>" href="/">inicio</a>
                            </li>
                            <li class="nav-item hide-lg">
                                <a class="nav-link <?php echo(checkActiveMenuOption('/about') ? 'active' : '') ?>" href="/about">conócenos</a>
                            </li>
                            <li class="nav-item hide-lg">
                                <a class="nav-link <?php echo(checkActiveMenuOption('/booktable') ? 'active' : '') ?>" href="/booktable">reservas</a>
                            </li>
                            <!-- Dropdown -->
                            <li class="nav-item">
                                <a class="nav-link <?php echo(checkActiveMenuOption('/gallery') ? 'active' : '') ?>" href="/gallery">galería</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                    @if(Auth::user())
                                    <?php echo explode(' ', Auth::user()->name)[0] ?>
                                </a>
                                <div class="dropdown-menu">
                                    @if (Auth::user()->hasRole('Admin'))
                                    <a class="dropdown-item" href="/administration">
                                        <i class="fa fa-cogs" aria-hidden="true"></i>
                                        Administrar
                                    </a>
                                    @endif
                                    <a class="dropdown-item" href="/user"><i class="fa fa-user" aria-hidden="true"></i> mi cuenta</a>
                                    @if (Auth::user()->hasRole('User'))
                                    <a class="dropdown-item" href="/user/reservations"><i class="fa fa-book" aria-hidden="true"></i> mis reservas</a>
                                    @endif
                                    <a class="dropdown-item" href="/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> cerrar sesión</a>
                                </div>
                                @else
                                entrar</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/login"><i class="fa fa-sign-in" aria-hidden="true"></i> iniciar sesión</a>
                                    <a class="dropdown-item" href="/register"><i class="fa fa-user-plus" aria-hidden="true"></i> registro</a>
                                </div>
                                @endif
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
<!--================ End Header Area =================-->