<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Reserva L'Assaig Batoi</title>
</head>
<body>

<p>Has realizado una reserva en la web de L'Assaig Batoi.</p>
<p>Datos de la reserva</p>
<ul>
  <li><b>Nombre: </b>{{$name}}</li>
  <li><b>Dia y hora: </b>{{$date}} - {{$time}}</li>
</ul></body>
</html>