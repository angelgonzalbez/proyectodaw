<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <title>Reserva L'Assaig Batoi</title>
    </head>
    <body>

        <p>Hola,</p>
        <p>{{$mensaje}}</p>
        <p>Le adjuntamos los datos de la reserva:</p>
        <ul>
            <li><b>Nombre: </b>{{$name}}</li>
            <li><b>Dia y hora: </b>{{$date}} - {{$time}}</li>
            <li><b>Personas: </b>{{$assistants}}</li>
        </ul>
        <p>Para modificar o cancelar su reserva entre aquí:</p>
        <a href="{{$link}}">
            <button class="btn btn-primary">ACCEDER</button>
        </a>
        <p>Copie y pege el link en su navegador si no le funciona el botón.</p>
        <p>{{$link}}</p>
    </body>
</html>