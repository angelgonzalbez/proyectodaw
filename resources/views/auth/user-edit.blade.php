@extends('layouts.master')
@section('content')
<div class="row justify-content-center">
    <div class="col-md-offset-3 col-md-6 mt-120">

        @if(Session::has('flash_message'))
        <div class="alert alert-success text-center"><span class="fa fa-check" aria-hidden="true"> {!! session('flash_message') !!}</span></div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-center">
                    <span class="fa fa-user" aria-hidden="true"></span>
                    Mi cuenta
                </h3>
            </div>
            <div class="panel-body" style="padding:30px">
                <form method='POST'>
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for='name'>Nombre:</label>
                        <input type='text' name='name' id="name" value="{{$user->name}}" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" maxlength="40">
                        @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail:</label>
                        <input type="text" name="email" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{$user->email}}" maxlength="40">
                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="phone">Teléfono:</label>
                        <input type="tel" name="telephone" id="telephone" class="form-control {{ $errors->has('telephone') ? ' is-invalid' : '' }}" value="{{$user->telephone}}" maxlength="9">
                        @if ($errors->has('telephone'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('telephone') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group text-center">
                        <button type="submit" onclick="return confirm('Seguro que quieres modificar los datos?');" id="btnEditUser" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                            Editar información
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop