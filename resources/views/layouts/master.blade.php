<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="no-js">

    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->
        <link rel="shortcut icon" href="/img/favicon.png">
        <!-- Author Meta -->
        <meta name="author" content="CodePixar">
        <!-- Meta Description -->
        <meta name="description" content='L\'Assaig restaurant web page'>
        <!-- Meta Keyword -->
        <meta name="keywords" content="L\'Assaig">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- meta token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @if(Auth::user())
        @if(Auth::user()->hasRole('Admin'))
        <meta name="user-token" content="{{Auth::user()->api_token}}">
        @endif
        @endif
        <!-- Site Title -->
        <title>{{ config('app.name', 'Laravel') }}</title>

        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i|Roboto:400,500" rel="stylesheet">
        <!--CSS=============================================-->
        <link rel="stylesheet" href="/css/bootstrap.css">
                <link rel="stylesheet" href="/css/bootstrap.css">
        <link rel="stylesheet" href="/css/linearicons.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">

        <link rel="stylesheet" href="/css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/magnific-popup.css">
        <link rel="stylesheet" href="/css/selection.css">
        <!---------------------------------------------------------->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!---------------------------------------------------------->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

    </head>

    <body>

        @include('partials.header')
        <main>
            @yield('content')
        </main>
        @include('partials.footer')
        <script src="/js/vendor/jquery-2.2.4.min.js"></script>
        <script src="/js/popper.min.js"
        crossorigin="anonymous"></script>
        <script src="/js/vendor/bootstrap.min.js"></script>
        <script src="/js/main.js"></script>
        <script src="/js/bmodal.js"></script>
        <script src="/js/jquery.magnific-popup.min.js"></script>
        <script src="/js/bootstrap-datepicker.js"></script>
        <script src="/js/reservation.js"></script>
        <script type="text/javascript" src="/js/selection.js"></script>
        <script type='text/javascript'>
_WidgetManager._Init();
_WidgetManager._RegisterWidget('_BlogArchiveView', new _WidgetInfo('BlogArchive1', 'sidebar-right-2-2', document.getElementById('BlogArchive1'), {'languageDirection': 'ltr', 'loadingMessage': 'S\x27està carregant\x26hellip;'}, 'displayModeFull'));
        </script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="/js/tableScript.js"></script>

    </body>

</html>