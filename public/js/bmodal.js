var SERVER = "http://localhost/api";

$(document).ready(function () {
    let url = "" + window.location;
    if (url.includes("Reservations")) { // User reservations
        
        setListeners();
        hideModalUnique();
    }
    
    if(url.includes("reset")){ // Reset Reservation
        
        setListenersTwoButtons(0);
        hideModal();
    }
});
// Modify/Cancel Reservations - Functions

// Set onClick listeners
function setListeners() {
    // LAUNCH FORM
    Array.from(document.getElementsByTagName('button')).forEach(celda => {
        if (celda.id > 0) {
            celda.addEventListener('click', launchModals);

        }
        if (celda.id.charAt(0) === 'c') { //Add listener to cancel buttons
            celda.addEventListener('click', launchModals);
        }
    });
}

// Hide modals
function hideModal() {
    $('#reservationModal').on('hidden.bs.modal', function (e) {
        document.getElementsByClassName("modal-body")[0].innerHTML = "";
    });
    $('#alertReservationModal').on('hidden.bs.modal', function (e) {
        document.getElementById("alertModal").innerHTML = "";
        document.getElementById("alertModalFooter").innerHTML =
                "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Cerrar</button>";
    });
}

function hideModalUnique() {
    $('#reservationModal').on('hidden.bs.modal', function (e) {
        document.getElementsByClassName("modal-body")[0].innerHTML = "";
        document.getElementsByClassName("modal-footer")[0].innerHTML =
                "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Cerrar</button>";
    });
}

// Launch
function launchModals(event) {
    event.preventDefault();
    if(event.target.id.charAt(0) === 'c'){ // Cancel
        textModal(event.target.id.substring(1), "¿Estas seguro de cancelar la reserva?");
    } else { // Edit
        setForm(event.target.id);
    }
}

// Get ajax responses
// Cancel response
function cancelReserve(event) {
    event.preventDefault();
    deleteReservation(event.target.id.substring(2))
            .then(function (response) {
                if(response){
                    document.getElementsByClassName("modal-body")[0].innerHTML = 
                            "<p>RESERVA CANCELADA CORRECTAMENTE";
                    deleteFromTable(event.target.id.substring(2));
                } else{
                    document.getElementById('cc'+event.target.id.substring(2)).remove();
                    document.getElementsByClassName("modal-body")[0].innerHTML = 
                            "<p>ERROR AL CANCELAR LA RESERVA";
                }
                admWait();
            })
            .catch(function (error) {
                $('#reservationModal').modal('hide');
                alert(error + ". Algo ha pasado, intentelo de nuevo más tarde.");
            });
}

// Get user reservation promise
function setForm(id) {
    getReservation(id)
            .then(function (reservation) {
                
                let form = getForm(0,reservation[0].id);
                $('#reservationModal').modal({
                    keyboard: true
                });
                document.getElementsByClassName("modal-body")[0].innerHTML = form;
                document.getElementById("modalFooter").innerHTML = '';
                // LOAD DATA
                document.getElementById("name").value = reservation[0].name;
                document.getElementById("email").value = reservation[0].email;
                document.getElementById("phone").value = reservation[0].telephone;
                document.getElementById("personas").value = reservation[0].assistants;
                document.getElementById("comentarios").value = reservation[0].observations;
                getMaxAsistants(reservation[0].idReservation)
                        .then(function (max){
                            let maxium = max+ reservation[0].assistants;
                    document.getElementById('personas').setAttribute('max',maxium);
                    document.getElementsByTagName('form')[0].addEventListener('submit',sendEditedReservation);
               })
                .catch(function (error){
                    $('#reservationModal').modal('hide');
                    alert(error + " :Algo ha pasado, intentelo de nuevo más tarde.");
                });
            })
            .catch(function (error) {
                $('#reservationModal').modal('hide');
                alert(error + " :Algo ha pasado, intentelo de nuevo más tarde.");
            });
}

function sendEditedReservation(event){
    event.preventDefault();
    
   saveModifiedReservation(event.target.id.substring(4))
            .then( function(response){
                if(response){
                    updateTable(event.target.id.substring(4));
                    document.getElementById('btnSave').remove();
                    document.getElementsByClassName("modal-footer")[0].innerHTML = 
                            "<p class='text-center'>RESERVA MODIFICADA CORRECTAMENTE";
                    admWait(); 
                } else{
                    document.getElementById('modalFooter').innerHTML = '';
                    document.getElementsByClassName("modal-footer")[0].innerHTML = 
                            "<p class='text-center'>ERROR AL MODIFICAR LA RESERVA";
                }      
    })
    .catch (function(error) {
        $('#reservationModal').modal('hide');
        alert(error + " :Algo ha pasado, intentelo de nuevo más tarde.");
    });
}

function updateTable(id){
    let data = getData();
    let table = document.getElementById('tr'+id).children;
    table[0].innerHTML = data['name'];
    table[1].innerHTML = data['email'];
    table[2].innerHTML = data['phone'];
    table[3].innerHTML = data['assistants'];
    table[5].innerHTML = data['comments'];
}

function getData(){
    let form = document.getElementsByTagName('form')[0];
    let petition = {
        name: form[0].value,
        email: form[1].value,
        phone: form[2].value,
        assistants: form[3].value,
        comments: form[4].value
    };
    return petition;
}

// Ajax promises
// Get User reservation promise
function getReservation(id) {
    return new Promise(function (resolve, reject) {
        let peticion = new XMLHttpRequest();
        peticion.open('GET', SERVER + '/reservation/' + id);
        peticion.setRequestHeader('Authorization','Bearer '+ $('meta[name="user-token"]').attr('content'));
        peticion.send();
        peticion.addEventListener('load', function () {
            if (peticion.status === 200) {
                resolve(JSON.parse(peticion.responseText));
            } else {
                reject("Error " + this.status + " (" + this.statusText + ") en la petición");
            }
        });
        peticion.addEventListener('error', () => reject('Error en la petición HTTP'));
    });
}

// Save modified reservation
function saveModifiedReservation(id) {
    let petition = getData();
    return new Promise(function (resolve, reject) {
        let peticion = new XMLHttpRequest();
        peticion.open('PUT', SERVER + '/reservation/edit/' + id);
        peticion.setRequestHeader('Authorization','Bearer '+ $('meta[name="user-token"]').attr('content'));
        peticion.send(JSON.stringify(petition));
        peticion.addEventListener('load', function () {
            if (peticion.status === 200) {
                resolve(JSON.parse(peticion.responseText));
            } else {
                reject("Error " + this.status + " (" + this.statusText + ") en la petición");
            }
        });
        peticion.addEventListener('error', () => reject('Error en la petición HTTP'));
    });
}

//Delete user reservation promise
function deleteReservation(id) {
    return new Promise(function (resolve, reject) {
        let peticion = new XMLHttpRequest();
        peticion.open('DELETE', SERVER + '/reservation/' + id);
        peticion.setRequestHeader('Authorization','Bearer '+ $('meta[name="user-token"]').attr('content'));
        peticion.send();
        peticion.addEventListener('load', function () {
            if (peticion.status === 200) {
                resolve(JSON.parse(peticion.responseText));
            } else {
                reject("Error " + this.status + " (" + this.statusText + ") en la petición");
            }
        });
        peticion.addEventListener('error', () => reject('Error en la petición HTTP'));
    });
}

// Get reservation max assistants
function getMaxAsistants(id) {
    return new Promise(function (resolve, reject) {
        let peticion = new XMLHttpRequest();
        peticion.open('GET', SERVER + '/reservation/assistants/max/' + id);
        peticion.setRequestHeader('Authorization','Bearer '+ $('meta[name="user-token"]').attr('content'));
        peticion.send();
        peticion.addEventListener('load', function () {
            if (peticion.status === 200) {
                resolve(JSON.parse(peticion.responseText));
            } else {
                reject("Error " + this.status + " (" + this.statusText + ") en la petición");
            }
        });
        peticion.addEventListener('error', () => reject('Error en la petición HTTP'));
    });
}

// FUNCTIONS
// Get edit user reservation form
function getForm(num,id) {
    if (num === 0) {
        return "<form id='form"+id+"'>" +
                "<div class='form-group'>" +
                "<label for='name'>Nombre reserva</label>" +
                "<input type='name' class='form-control' id='name' aria-describedby='Name' placeholder='Introduce el nombre' required>" +
                "</div>" +
                "<div class='form-group'>" +
                "<label for='email'>Email</label>" +
                "<input type='email' class='form-control' id='email' aria-describedby='Email' placeholder='Introduce el email' required>" +
                "</div>" +
                "<div class='form-group'>" +
                "<label for='phone'>Teléfono</label>" +
                "<input type='text' pattern='[0-9]{9}' class='form-control' id='phone' aria-describedby='Teléfono' placeholder='Introduce el teléfono' required>" +
                "</div>" +
                "<div class='form-group'>" +
                "<label for='personas'>Personas</label>" +
                "<input type='number' class='form-control' id='personas' min='1' step='1' required>" +
                "</div>" +
                "<div class='form-group'>" +
                "<label for='comentarios'>Comentarios</label>" +
                "<input type='text' class='form-control' id='comentarios'>" +
                "</div>" +
                "<button type='button' class='btn btn-secondary' data-dismiss='modal'>Cerrar</button>" +
                "<button id='btnSave' type='submit' class='btn btn-primary'>Guardar</button>" +
                "</form>";
    } else {
        return "<form>" +
                "<div class='form-group'>" +
                "<label for='email'>Email</label>" +
                "<input type='email' class='form-control' id='email' aria-describedby='Email' placeholder='Introduce el email'>" +
                "<small id='email' class='form-text text-muted'>Este campo es para comprobar que realmente eres tu</small>" +
                "</div>" +
                "</form>";
    }
}
// Launch alert modal
function textModal(idText, text) {
    $('#reservationModal').modal({
        keyboard: true
    });
    document.getElementsByClassName("modal-body")[0].innerHTML += "<p>" + text + "</p>";
    
    document.getElementById("modalFooter").innerHTML +=
            "<button id=\'cc" + idText + "\' class='btn btn-danger' style='display:inline'>Cancelar</button>";
    
    document.getElementById("cc" + idText).addEventListener('click', cancelReserve);
}

function admWait(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
async function admWait() {
  await sleep(1500);
    $('#reservationModal').modal('hide');
}

function deleteFromTable(id){ // Delete file from table
    document.getElementById('tr'+id).remove();
}

// Reset user reservation

// Add listeners to the save and cancel button
function setListenersTwoButtons($id) {
    if ($id === 0) {
        Array.from(document.getElementsByTagName('button')).forEach(celda => {
            if (celda.id === 'save') {
                celda.addEventListener('click', setConfirmationButton);
                
            } else {
                celda.addEventListener('click', setConfirmationButton);
            }
        });
    } else {
        document.getElementById($id).addEventListener('click', setAction);
    }
}
//method="POST" action="/reset-reservation/reset"
function setConfirmationButton(event) {
    event.preventDefault();
    launchModal(event.target.id);
}

function launchModal($id) {
    $('#alertReservationModal').modal({
        keyboard: true
    });
    
    let form = getForm(1);
    document.getElementsByClassName("modal-body")[0].innerHTML += form;

    if ($id === 'save') {
        document.getElementById("alertModalFooter").innerHTML +=
                "<button id='bsave' class='btn btn-primary' style='display:inline'>Guardar</button>";
        setListenersTwoButtons('bsave');
    } else {
        document.getElementById("alertModalFooter").innerHTML +=
                "<button id='bcancel' class='btn btn-danger' style='display:inline'>Cancelar reserva</button>";
        setListenersTwoButtons('bcancel');
    }
    
}

function validateEmail(email){
     var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
     return re.test(String(email).toLocaleLowerCase());
}

function setAction(event){
    event.preventDefault();
    let email = document.getElementById('email').value;
    if(validateEmail(email)){
        let failEmail = document.getElementsByClassName("text-danger")[0];
        if(failEmail){
            failEmail.remove();
        }
        if(event.target.id === 'bsave'){
            launchPetitionModify(email);
        } else {
            launchPetitionCancel(email);
        }
    } else {
        if(!document.getElementsByClassName("text-danger")[0]){
            document.getElementsByClassName("modal-body")[0].innerHTML += "<p class='text-danger'>Email incorrecto</p>";
        } 
    }
}

function launchPetitionModify(email){
    let tk = document.getElementById('token').value;
    if(checkData()){
        let petition = {
            token:tk,
            mail: email,
            name: document.getElementById('name').value,
            phone: document.getElementById('telephone').value,
            assistants: document.getElementById('assistants').value,
            obs: document.getElementById('observations').value
        };
    modifyReservation(petition)
            .then(function (response) {
                if(response){
                    $('#alertReservationModal').modal('hide');
                    window.location.replace("http://localhost/reservation/modified");
                } else {
                    wait();
                    if(document.getElementsByClassName("text-danger")[0]){
                        document.getElementsByClassName("modal-body")[0].innerHTML = "<p class='text-danger'>NO SE HA PODIDO REALIZAR LA MODIFICACIÓN</p>";
                    } else{
                        document.getElementsByClassName("modal-body")[0].innerHTML += "<p class='text-danger'>NO SE HA PODIDO REALIZAR LA MODIFICACIÓN</p>";
                    }
                }
            })
            .catch(function (error) {
                $('#alertReservationModal').modal('hide');
                console.log(error + " :Algo ha pasado, intentelo de nuevo más tarde.");
            });
        } else {
            wait();
            document.getElementsByClassName("modal-body")[0].innerHTML += "<p class='text-danger'>INTRODUCE CORRECTAMENTE LOS DATOS</p>";
        }
}

function launchPetitionCancel(email){
    let tk = document.getElementById('token').value;
    let petition = { 
        token:tk,
        mail: email
    };
    cancelReservation(petition)
            .then(function (response) {
                if(response){
                    $('#alertReservationModal').modal('hide');
                    window.location.replace("http://localhost/reservation/canceled");
                } else {
                    wait();
                    if(document.getElementsByClassName("text-danger")[0]){
                        document.getElementsByClassName("modal-body")[0].innerHTML = "<p class='text-danger'>NO SE HA PODIDO REALIZAR LA CANCELACIÓN DE LA RESERVA</p>";
                    } else{
                        document.getElementsByClassName("modal-body")[0].innerHTML += "<p class='text-danger'>NO SE HA PODIDO REALIZAR LA CANCELACIÓN DE LA RESREVA</p>";
                    }
                }
            })
            .catch(function (error) {
                $('#alertReservationModal').modal('hide');
                console.log(error + " :Algo ha pasado, intentelo de nuevo más tarde.");
            });
}

function cancelReservation(petition) {
    return new Promise(function (resolve, reject) {
        let peticion = new XMLHttpRequest();
        peticion.open('DELETE', SERVER + '/reset-reservation/cancel');
        peticion.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
        peticion.send(JSON.stringify(petition));
        peticion.addEventListener('load', function () {
            if (peticion.status === 200) {
                resolve(JSON.parse(peticion.responseText));
            } else {
                reject("Error " + this.status + " (" + this.statusText + ") en la petición");
            }
        });
        peticion.addEventListener('error', () => reject('Error en la petición HTTP'));
    });
}

function modifyReservation(petition){
    return new Promise(function (resolve, reject) {
        let peticion = new XMLHttpRequest();
        peticion.open('PUT', SERVER + '/reset-reservation/modify');
        peticion.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
        peticion.send(JSON.stringify(petition));
        peticion.addEventListener('load', function () {
            if (peticion.status === 200) {
                resolve(JSON.parse(peticion.responseText));
            } else {
                reject("Error " + this.status + " (" + this.statusText + ") en la petición");
            }
        });
        peticion.addEventListener('error', () => reject('Error en la petición HTTP'));
    });
}

function checkData(){
    let expresionRegular1=/^([0-9]+){9}$/;
    let expresionRegular2=/\s/;
    let correct = true;
    
    if(document.getElementById('name').value.length <= 0){
        correct = false;
    }
    if(!Number(document.getElementById('telephone').value)){
       correct = false;
    }
    if(document.getElementById('telephone').value.length !== 9){
           correct = false;
       }
    
    if(!Number(document.getElementById('assistants').value) > 0){
        correct = false;
    }
    
    return correct;
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
async function wait() {
  await sleep(1500);
    $('#alertReservationModal').modal('hide');
}