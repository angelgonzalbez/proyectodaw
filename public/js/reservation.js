'use strict';

function setMaxDate() {
    let date = document.getElementsByName('fecha_final')[0].value;
    if (date) {
        console.log(date);
        document.getElementsByName('date')[0].max = date;
    }

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    today = yyyy + '-' + mm + '-' + dd;

    document.getElementsByName('date')[0].min = today;

}

function setMinDate() {
    if (document.getElementsByName('fecha_final')[0]) {
        if (document.getElementsByName('date')[0].value) {
            let date = document.getElementsByName('date')[0].value;
            if (date) {
                document.getElementsByName('fecha_final')[0].min = date;
            }
        } else {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }

            today = yyyy + '-' + mm + '-' + dd;

            document.getElementsByName('fecha_final')[0].min = today;
        }

    } else {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        today = yyyy + '-' + mm + '-' + dd;

        document.getElementsByName('date')[0].min = today;
    }
}

function cambiarFormReserva() {
    if (document.getElementById('changeForm').checked) {
        document.getElementById('change').innerHTML = '<div class="row"><label class="offset-lg-6 col-lg-2 d-lg-block d-none">' +
                'Repetir cada:</label>' +
                '<div class="col-lg-6">' +
                '<input type="checkbox" id="changeForm" name="changeForm" onclick="cambiarFormReserva()" checked>' +
                ' Crear varias reservas</div>' +
                '<label class="col-12 d-lg-none">' +
                'Repetir cada:</label>' +
                '<div class="input-group col-lg-6 mb-20">' +
                '<select name="repetir" class="form-control">' +
                '<option value="lunes">' +
                'Todos los lunes</option>' +
                '<option value="martes">' +
                'Todos los martes</option>' +
                '<option value="miercoles">' +
                'Todos los miércoles</option>' +
                '<option value="jueves">' +
                'Todos los jueves</option>' +
                '<option value="viernes">' +
                'Todos los viernes</option>' +
                '<option value="semana">' +
                'Todos los dias entre semana</option>' +
                '</select>' +
                '<span class="input-group-append">' +
                '<button class="btn btn-outline-secondary border-left-0 border-0" type="button">' +
                '<i class="fa fa-repeat">' +
                '</i>' +
                '</button>' +
                '</span>' +
                '</div>' +
                '<label for="fecha_inicio" class="col-lg-4">' +
                'Fecha inicio:</label>' +
                '<label for="fecha_final" class="col-lg-4 d-lg-block d-none">' +
                'Fecha final:</label>' +
                '<label for="hora" class="col-lg-4 d-lg-block d-none">' +
                'Hora:</label>' +
                '<div class="input-group col-lg-4 mb-20">' +
                '<input name="date" onfocus="setMaxDate()" class="form-control" class="form-control" required="" type="date">' +
                '<span class="input-group-append">' +
                '<button class="btn btn-outline-secondary border-left-0 border-0" type="button">' +
                '<i class="fa fa-calendar reservation-calendar">' +
                '</i>' +
                '</button>' +
                '</span>' +
                '</div>' +
                '<label for="fecha_final" class="col-12 d-lg-none">' +
                'Fecha final:</label>' +
                '<div class="input-group col-lg-4 mb-20">' +
                '<input name="fecha_final" onfocus="setMinDate()" class="form-control" class="form-control" required="" type="date">' +
                '<span class="input-group-append">' +
                '<button class="btn btn-outline-secondary border-left-0 border-0" type="button">' +
                '<i class="fa fa-calendar reservation-calendar">' +
                '</i>' +
                '</button>' +
                '</span>' +
                '</div>' +
                '<label for="hora" class="col-12 d-lg-none">' +
                'Hora:</label>' +
                '<div class="input-group col-lg-4 mb-20">' +
                '<input name="timetable" placeholder="Hora" value="14:00" class="form-control" required="" type="time">' +
                '<span class="input-group-append">' +
                '<button class="btn btn-outline-secondary border-left-0 border-0" type="button">' +
                '<i class="fa fa-clock-o">' +
                '</i>' +
                '</button>' +
                '</span>' +
                '</div></div>';
    } else {
        document.getElementById('change').innerHTML = '<input type="hidden" id="changeFormHidden" name="changeForm">' +
                '<input type="checkbox" id="changeForm" name="changeForm" onclick="cambiarFormReserva()"> Crear varias reservas' +
                '<div class="row"><label for="fecha_inicio" class="col-lg-6">' +
                'Fecha:</label>' +
                '<label for="hora" class="col-lg-6 d-lg-block d-none">' +
                'Hora:</label>' +
                '<div class="input-group col-lg-6 mb-20">' +
                '<input name="date" onfocus="setMinDate()" class="form-control" class="form-control" required="" type="date">' +
                '<span class="input-group-append">' +
                '<button class="btn btn-outline-secondary border-left-0 border-0" type="button">' +
                '<i class="fa fa-calendar reservation-calendar">' +
                '</i>' +
                '</button>' +
                '</span>' +
                '</div>' +
                '<label for="timetable" class="col-12 d-lg-none">' +
                'Hora:</label>' +
                '<div class="input-group col-lg-6 mb-20">' +
                '<input name="timetable" placeholder="Hora" value="14:00" class="form-control" required="" type="time">' +
                '<span class="input-group-append">' +
                '<button class="btn btn-outline-secondary border-left-0 border-0" type="button">' +
                '<i class="fa fa-clock-o">' +
                '</i>' +
                '</button>' +
                '</span>' +
                '</div></div>';
    }
}