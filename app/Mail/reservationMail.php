<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class reservationMail extends Mailable
{
    use Queueable, SerializesModels;
 
    public $name;
    public $time;
    public $date;
    public $mensaje;
    public $tipo;
    public $link;
    public $assistants;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $time, $date,$assistants, $mensaje,$link,$tipo=1)
    {
        $this->name = $name;
        $this->time = $time;
        $this->date = $date;
        $this->mensaje = $mensaje;
        $this->tipo = $tipo;
        $this->link = $link;
        $this->assistants = $assistants;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->tipo === 0){
            
            return $this->view('mail.reservationAlert');  
        }
        return $this->view('mail.reservationDone');
    }
}