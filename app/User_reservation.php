<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_reservation extends Model
{
    protected $fillable = [
        'assistants', 'observations', 'name', 'email', 'telephone', 'EnEspera'
    ];
    
    public function reservation() {
        return $this->hasOne('App\Reservation','id','idReservation');
    }
}