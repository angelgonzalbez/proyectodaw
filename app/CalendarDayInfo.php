<?php

namespace App;

class CalendarDayInfo {

    var $reservationId;
    var $date;
    var $color;
    var $spaces;
    var $onWaitingList;

    function __construct($reservationId, $date, $color, $spaces=null, $onWaitingList=false){
        $this->reservationId = $reservationId;
        $this->date = $date;
        $this->color = $color;
        $this->spaces = $spaces;
        $this->onWaitingList = $onWaitingList;
    }

    function getInfo()
    {/*
       return {
        start: '2019-02-01',
        end: '2019-02-02',
        backgroundColor: 'red',
        rendering: 'background'
       }*/
    }
    function getStartDate(){
        return $this->date;
    }
    function getEndDate(){
        return $this->date;
    }
    function getBackgroundColor(){
        return "$this->color";
    }
    function getSpaces(){
        return $this->spaces;
    }
    function onWaitingList(){
        if ($this->onWaitingList){
            return true;
        }else {
            return 0;
        }
    }
    function checkSpaces(){
        if ($this->spaces != null){
            return true;
        }
    }
    function getReservationId(){
        return $this->reservationId;
    }
    function checkReservable($date){
        if ($date == $this->date){
            return true;
        }else {
            return false;
        }
    }
    function getAllInfo(){
        $cadena = "start:".$this->getStartDate().",
        end:".$this->getEndDate().",
        backgroundColor:".$this->getBackgroundColor().",
        rendering: background";
        return $cadena;
    }


}