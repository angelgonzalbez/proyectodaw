<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation_teacher extends Model
{
    protected $fillable = [];
    public $timestamps = false;
    
    public function reservation() {
        return $this->hasOne('App\Reservation','id','idReservation');
    }
}
