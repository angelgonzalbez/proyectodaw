<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User_reservation;
use App\Reservation;

class StoreReservation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $reservation = Reservation::where('id', $this->reservationId)->first();
        $userReservations = User_reservation::where('idReservation', $this->reservationId)->get();
        $assistantsNumber = 0;
        $plazasDisponibles = 0;
        foreach ($userReservations as $userReservation) {
            $dateReservation = Reservation::findOrFail($userReservation->idReservation)->date;
            $assistantsNumber += $userReservation->assistants;
        }
        
        if($assistantsNumber < $reservation->spaces) {
            $plazasDisponibles = $reservation->spaces + $reservation->overbooking - $assistantsNumber;
        }
        if($plazasDisponibles == 0) {
            $plazasDisponibles = $reservation->waitingList;
            
        }
        
        return [
            'name' => 'required|max:50',
            'email' => 'email',
            'telephone' => 'required|numeric|digits:9',
            'assistants' => 'required|numeric|min:0|max:'.$plazasDisponibles
        ];
    }
}
