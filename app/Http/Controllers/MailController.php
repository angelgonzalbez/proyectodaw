<?php

namespace App\Http\Controllers;

use Mail;
use App\Mail\reservationMail;

class MailController extends Controller {
    

    public static function sendReservationMail($email, $name, $time, $date,$assistants, $mensaje, $tipo, $link){
        Mail::to($email)->send(new reservationMail($name, $time, $date,$assistants,$mensaje,$link,$tipo));
    }
}