<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
/**
 * Description of UserController
 *
 * @author Iñaki Peidro
 */
class UserController {
        public function edit(Request $request) {
        
        $user = Auth::user();
        
        $request->validate([
            'name'     => 'required|string',
            'email'    => 'required|string|email',
            'telephone' => 'required|digits:9'
        ]);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->telephone = $request->telephone;
        $user->save();
        
        \Session::flash('flash_message','LOS DATOS DEL USUARIO SE HAN ACTUALIZADO CORRECTAMENTE'); 

        return view('auth.user-edit', compact('user'))->with('message', 'Success!'); 
    }
}
