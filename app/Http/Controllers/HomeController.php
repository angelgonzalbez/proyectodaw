<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;
use App\User_reservation;
use Illuminate\Support\Facades\Auth;
use App\Teacher;

// Only works when you're autenticated
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect('/');
    }
 
    public function getAdminPanel(){
        
        return view('admin-panel.index');

    }
    
    public function getUserEditPanel(){
        
        $user = Auth::user();
        \Session::flash('flash_message',null); 
        return view('auth.user-edit', compact('user'));
    }
    
    public function getUserReservations(){
        
        $reservations = User_reservation::all();
        \Session::flash('flash_message','No hay reservas que mostar'); 
        return view('reservation.user-history', compact('reservations'));
    }

    public function createTeacher(){
        return view('admin-panel.createTeacher');
    }

    public function createTeacherDone(Request $request){
        $teacher = new Teacher();
        $teacher->name = $request->input('name');
        $teacher->department = $request->input('department');
        $teacher->save();
        return redirect("administration/teacherList");
    }

    public function teacherList(){
        $teachersArray = Teacher::all();
        return view('admin-panel.teachersList', compact('teachersArray'));
    }

    public function editTeacherForm($id){
        $teacher = Teacher::findOrFail($id);
        $name = $teacher->name;
        $department = $teacher->department;
        return view('admin-panel.editTeacher', compact('name', 'department', 'id'));
    }

    public function deleteTeacher($id){
        $teacher = Teacher::findOrFail($id);
        $teacher->delete();
        return redirect("administration/teacherList");
    }

    public function editTeacherDone(Request $request){
        $teacher = Teacher::findOrFail($request->input('id'));
        $teacher->name = $request->input('name');
        $teacher->department = $request->input('department');
        $teacher->save();
        return redirect("administration/teacherList");
    }
}
