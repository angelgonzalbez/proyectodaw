<?php

namespace App\Http\Controllers\Reservation;

use Illuminate\Http\Request;
use App\Reset_reservation;
use App\User_reservation;
use App\Reservation;

class ResetReservationController extends \App\Http\Controllers\Controller {

    public function get($token) {
        
        $reset_reservation = Reset_reservation::where('token', $token)->first();
        
        if ($reset_reservation) {
            
            $user_reservation = User_reservation::where('id', decrypt($token))->first();
            if($user_reservation) { // Check no error, if reservation deleted
                $nombre = $user_reservation->name;
                $telefono = $user_reservation->telephone;
                $persons = $user_reservation->assistants;
                $maxAssistants = self::getAssistantsNumber($persons,$user_reservation->idReservation);
                $comment = $user_reservation->observations;
                return view('reservation.reset', 
                        compact('maxAssistants', 'nombre', 'telefono','token','persons','comment'));
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    public function post(Request $request){
        
        if(self::checkResetResponse($request->token,$request->email)){
           // self::checkDeleteResetLink($request->email); // Delete reset link
            $user_reservation = User_reservation::where('id', decrypt($request->token))->first();
            if(self::checkAssistants($request->assistants,$user_reservation)){
                $user_reservation->name = $request->name;
                $user_reservation->telephone = $request->telephone;
                $user_reservation->assistants = $request->assistants;
                $user_reservation->observations = $request->observations;
                $user_reservation->save();
                
                self::resetDone("Se ha modificado correctamente su reserva");
            }
        }
    }
    
    public function cancel(Request $request) {
        //Todo alert
        if(self::checkResetResponse($request->token,$request->email)){
            $user_reservation = User_reservation::where('id', decrypt($request->token))->first();
            $user_reservation->delete();
            
             self::resetDone("Has cancelado tu reserva");
        }
    }
    
    private static function resetDone($message){
        
        return view('reservation.alert', compact('message'));
    }

    public static function makeResetLink($user_reservation) {
        self::checkDeleteResetLink($user_reservation->email);
        
        $reset_reservation = new Reset_reservation();
        $reset_reservation->email = $user_reservation->email;
        $reset_reservation->token = encrypt($user_reservation->id);
        $reset_reservation->created_at = date('Y-m-d');
        $reset_reservation->save();

        return self::sendLink($reset_reservation->token);
    }

    /** STATIC METHODS **/
    
    private static function sendLink($token) {

        return url('/') . '/reservation/reset/' . $token;
    }

    public static function checkDeleteResetLink($email) {
        $reset_reservation = Reset_reservation::where('email', $email)->first();
        if ($reset_reservation) {
            $reset_reservation->delete();
        }
    }
    
    public static function checkResetResponse($token, $email){
        $reset_reservation = Reset_reservation::where([
            ['token', $token],
            ['email',$email]
                ])->first();
        
        if($reset_reservation) {
            return true; // Exists
        }
        return false; // No exists
    }
    
    public static function getAssistantsNumber($user_assistants,$id){
        
        $reservation = Reservation::findOrFail($id);
        
        $assistants = ($reservation->spaces) + ($reservation->overbooking);

        $userReservations = User_reservation::where('idReservation',$id)->get();
        
        foreach ($userReservations as $userReservation) {
            $assistants -= $userReservation->assistants;
        }
        
        return $assistants+$user_assistants;
    }
    
    public static function checkAssistants($assistants,$user_reservation){
        //Return true or false
    }
    
    /**
     * View of cancelled reservation
     */
    public function canceled(){
        
        return view('reservation.cancel');
    }
    
    public function modified(){
        
        return view('reservation.modify');
    }
}
