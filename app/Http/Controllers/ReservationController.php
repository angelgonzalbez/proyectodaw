<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;
use App\User_reservation;
use App\User;
use App\Teacher;
use App\Reservation_teacher;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use App\CalendarDayInfo;
use App\Http\Requests\StoreReservation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ReservationController extends Controller {

    public function makeReservation() {
        $profesores = Teacher::all();
        return view('reservation.index', compact('profesores'));
    }

    public function saveReservation() {
        ini_set('max_execution_time', 300);
        $profesoresCocina = $_POST['profesores-cocina'];
        $profesoresSala = $_POST['profesores-sala'];
        if ($_POST['changeForm']) {
            $repetir = $_POST['repetir'];
            $inicio = self::calcularInicio($repetir);
            $semana = intval(date('w', strtotime($inicio)));
            $final = $_POST['fecha_final'];
            if ($repetir != 'semana') {
                while ($inicio <= $final) {
                    self::createReservations($inicio, $profesoresCocina, $profesoresSala);
                    $inicio = date('Y-m-d', strtotime($inicio . "+1 week"));
                }
            } else {
                while ($inicio <= $final) {
                    self::createReservations($inicio, $profesoresCocina, $profesoresSala);
                    if ($semana < 5) {
                        $inicio = date('Y-m-d', strtotime($inicio . "+1 days"));
                        $semana++;
                    } else {
                        $semana = 1;
                        $inicio = date('Y-m-d', strtotime($inicio . "+3 days"));
                    }
                }
            }
        } else {
            $fecha = $_POST['date'];
            self::createReservations($fecha, $profesoresCocina, $profesoresSala);
        }
        return redirect('/');
    }

    public function calcularInicio($repetir) {
        $inicio = $_POST['date'];
        $semana = intval(date('w', strtotime($inicio)));
        switch ($repetir) {
            case 'lunes':
                if ($semana != 1) {
                    $resta = 8 - $semana;
                    $inicio = date('Y-m-d', strtotime($inicio . "+" . $resta . " days"));
                }
                break;
            case 'martes':
                if ($semana < 2) {
                    $resta = 2 - $semana;
                    $inicio = date('Y-m-d', strtotime($inicio . "+" . $resta . " days"));
                } else {
                    $resta = 9 - $semana;
                    $inicio = date('Y-m-d', strtotime($inicio . "+" . $resta . " days"));
                }
                break;
            case 'miercoles':
                if ($semana < 3) {
                    $resta = 3 - $semana;
                    $inicio = date('Y-m-d', strtotime($inicio . "+" . $resta . " days"));
                } else {
                    $resta = 10 - $semana;
                    $inicio = date('Y-m-d', strtotime($inicio . "+" . $resta . " days"));
                }
                break;
            case 'jueves':
                if ($semana < 4) {
                    $resta = 4 - $semana;
                    $inicio = date('Y-m-d', strtotime($inicio . "+" . $resta . " days"));
                } else {
                    $resta = 11 - $semana;
                    $inicio = date('Y-m-d', strtotime($inicio . "+" . $resta . " days"));
                }
                break;
            case 'viernes':
                if ($semana < 5) {
                    $resta = 5 - $semana;
                    $inicio = date('Y-m-d', strtotime($inicio . "+" . $resta . " days"));
                } else {
                    $resta = 12 - $semana;
                    $inicio = date('Y-m-d', strtotime($inicio . "+" . $resta . " days"));
                }
                break;
            case 'semana':
                if ($semana == 6 || $semana == 7) {
                    $resta = 8 - $semana;
                    $inicio = date('Y-m-d', strtotime($inicio . "+" . $resta . " days"));
                    $semana = 1;
                }
                break;
        }

        return $inicio;
    }

    public function createReservations($inicio, $profesoresCocina, $profesoresSala) {
        $reservation = new Reservation;
        $reservation->date = $inicio;
        $reservation->timetable = $_POST['timetable'];
        $reservation->spaces = $_POST['spaces'];
        $reservation->overbooking = $_POST['overbooking'];
        $reservation->waitingList = $_POST['waitingList'];
        $reservation->save();
        self::setTeachers($profesoresCocina, $profesoresSala, $inicio);
    }

    public function setTeachers($profesoresCocina, $profesoresSala, $fecha) {
        foreach ($profesoresCocina as $profe) {
            $reservation = Reservation::where('date', $fecha)->get();
            $resTeacher = new Reservation_teacher;
            $resTeacher->idTeacher = $profe;
            $resTeacher->idReservation = $reservation[0]->id;
            $resTeacher->save();
        }
        foreach ($profesoresSala as $profe) {
            $reservation = Reservation::where('date', $fecha)->get();
            $resTeacher = new Reservation_teacher;
            $resTeacher->idTeacher = $profe;
            $resTeacher->idReservation = $reservation[0]->id;
            $resTeacher->save();
        }
    }

    public function getIndexAdmin() {
        
    }

    public function getPastReservations() {
        $switch = true;
        $allReservations = User_reservation::all();
        $reservations = [];
        foreach ($allReservations as $reservation) {
            if (!fecha_mayor($reservation->reservation->date)) {
                array_push($reservations, $reservation);
            }
        }
        return view('reservation.history', compact('reservations', 'switch'));
    }

    public function getFutureReservations() {
        $switch = false;
        $allReservations = User_reservation::all();
        $reservations = [];
        foreach ($allReservations as $reservation) {
            if (fecha_mayor($reservation->reservation->date)) {
                array_push($reservations, $reservation);
            }
        }
        return view('reservation.history', compact('reservations', 'switch'));
    }

    public function getTodayReservationss() {
        $userReservations = User_reservation::all();
        $reservations = [];

        foreach ($userReservations as $userReservation) {
            $reservation = Reservation::findOrFail($userReservation->idReservation);
            if ($reservation->date == date("Y-m-d")) {
                array_push($reservations, $reservation);
            }
        }
        return view('reservation.history', compact('reservations'));
    }

    public function putReservation() {
        dd($this->getAssistantsNumberOnDate("2019-02-01"));
    }

    public function index() {
        ini_set('max_execution_time', 300);
        $events = [];
        $reservations = $this->getArrayForCalendar();
        $data = Reservation::all();

        if ($data->count()) {
            foreach ($data as $value) {

                $events[] = Calendar::event(
                                $value->id, true, new \DateTime($value->date), new \DateTime($value->date . ' +1 day'), null,
                                // Add color and link on event
                                [
                            'backgroundColor' => 'blue',
                            'url' => '/events',
                            'rendering' => 'background',
                                ]
                );
            }
        }
        $calendar = Calendar::addEvents($events);

        return view('reservation.fullcalendar', compact('calendar', 'reservations'));
    }

    public function createReservation() {
        $events = [];
        $reservations = $this->getArrayForCalendar();
        $data = Reservation::all();

        if ($data->count()) {
            foreach ($data as $value) {

                $events[] = Calendar::event(
                                $value->id, true, new \DateTime($value->date), new \DateTime($value->date . ' +1 day'), null,
                                // Add color and link on event
                                [
                            'backgroundColor' => 'blue',
                            'url' => '/events',
                            'rendering' => 'background',
                                ]
                );
            }
        }
        $calendar = Calendar::addEvents($events);

        return view('reservation.fullcalendar', compact('calendar', 'reservations'));
    }

    public function createReservationForm() {
        
    }

    public function calculateWaitingListFromId($reservationId) {
        $reservation = Reservation::findOrFail($reservationId);
        $userReservations = User_reservation::all();
        $assistantsNumber = 0;
        $plazasTotalesEnEspera = $reservation->waitingList;
        foreach ($userReservations as $userReservation) {
            $dateReservation = Reservation::findOrFail($userReservation->idReservation)->date;
            if ($dateReservation == $reservation->date && $userReservation->EnEspera) {
                $assistantsNumber += $userReservation->assistants;
            }
        }
        return $plazasTotalesEnEspera - $assistantsNumber;
    }

    public function getAssistantsNumberOnDate($date) {
        $reservation = Reservation::where('date', $date)->first();
        $userReservations = User_reservation::where('idReservation', $reservation->id)->get();
        $assistantsNumber = 0;
        foreach ($userReservations as $userReservation) {
            $dateReservation = Reservation::findOrFail($userReservation->idReservation)->date;
            $assistantsNumber += $userReservation->assistants;
        }
        return $assistantsNumber;
    }

    public function calculateBackgroundColor($date) {
        $numberOfAssistants = $this->getAssistantsNumberOnDate($date);
        $places = ReseReservation::findOrFail($userReservation->idReservation)->date;
    }

    public function getArrayForCalendar() {
        $reservations = Reservation::all();
        $calendarArray = [];

        foreach ($reservations as $reservation) {
            if (fecha_mayor($reservation->date)) {
                $reservationId = $reservation->id;
                $spaces = $reservation->spaces;
                $dateReservation = $reservation->date;
                $spacesLeft = ($spaces + $reservation->overbooking) - $this->getAssistantsNumberOnDate($dateReservation);

                if ($this->getAssistantsNumberOnDate($dateReservation) >= $spaces && $reservation->waitingList > 0) {

                    $dayInfo = new CalendarDayInfo($reservationId, $dateReservation, "yellow", $this->calculateWaitingListFromId($reservation->id), true);
                    array_push($calendarArray, $dayInfo);
                } else if ($this->getAssistantsNumberOnDate($dateReservation) >= $spaces) {

                    $dayInfo = new CalendarDayInfo($reservationId, $dateReservation, "red");
                    array_push($calendarArray, $dayInfo);
                } else if ($this->getAssistantsNumberOnDate($dateReservation) < $spaces) {

                    $dayInfo = new CalendarDayInfo($reservationId, $dateReservation, "green", $spacesLeft);
                    array_push($calendarArray, $dayInfo);
                }
            }
            //dd($reservation->waitingList);
        }
        return $calendarArray;
    }

    public function reservationForm() {
        Session::put('onWaitingList', $_POST["onWaitingList"]);
        Session::put('reservationId', $_POST["reservationId"]);
        return redirect('/reservationDone');
    }
    public function getReservationForm() {
        $onWaitingList = Session::get('onWaitingList');

        $reservation = Reservation::findOrFail(Session::get('reservationId'));
        $reservationId = $reservation->id;
        $assistants = $this->getAssistantsNumberOnDate($reservation->date);
        if ($assistants < $reservation->spaces) {
            $maxAssistants = $reservation->spaces - $assistants + $reservation->overbooking;
        } else {
            $maxAssistants = $this->calculateWaitingListFromId($reservationId);
        }
        $reservationId = Session::get('reservationId');
        if (Auth::user()) {
            $user = User::findOrFail(Auth::user()->id);
            $nombre = $user->name;
            $email = $user->email;
            $telefono = $user->telephone;
        } else {
            $nombre = "";
            $email = "";
            $telefono = "";
        }
        return view('reservation.reservationForm', compact('maxAssistants', 'reservationId', 'nombre', 'email', 'telefono', 'onWaitingList'));
    }

    public function reservationDone(StoreReservation $request) {
        $userReservation = new User_reservation();
        //$userReservation->idUser =
        $userReservation->idReservation = $request->reservationId;
        $userReservation->observations = $request->observations;
        $userReservation->name = $request->name;
        $userReservation->email = $request->email;
        $userReservation->telephone = $request->telephone;
        $userReservation->assistants = $request->assistants;
        $userReservation->waitingList = $request->onWaitingList;

        $reservation = Reservation::findOrFail($request->reservationId);
        $date = $reservation->date;
        $timetable = $reservation->timetable;
        $name = $request->name;
        $assistants = $request->assistants;
        //dd(Auth::user());
        
        $userReservation->save();
        return view('reservation.reservationDone', compact('name', 'assistants', 'date', 'timetable'));
    }
    
    public function accept($id){
        $user_reservation = User_reservation::findOrFail($id);
        $user_reservation->waitingList = 0;
        $user_reservation->save();
        return redirect('/administration/futureReservations');
    }

}
