<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreImage;
use App\Imagene;
use Image;

class ImageController extends Controller
{
    public function getIndex(){
        return view('image.index');
    }
    
    public function getGallery(){
        $fechas = getDateSelection();
        $mesesString = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $imagesArray = Imagene::paginate(3);
        
        return view('image.gallery', compact('imagesArray', 'fechas', 'mesesString'));
    }

    public function uploadGalleryImage(StoreImage $request){
        
        $ruta = public_path().'/img/gallery/';
        
        // Get form image
        $fileRequest = $request->file('file');
        
        // Create image
        $imagenToSave = Image::make($fileRequest);

        // Generate random name
        $name = random_string() . '.' . $fileRequest->getClientOriginalExtension();
        
        // Save( [ruta], [calidad])
        $imagenToSave->save($ruta . $name, 100);
        
        // Save image into bd
        $imagen = new Imagene();
        
        $imagen->name = $name;
           
        $imagen->date = date('Y-m-d');
        $imagen->save();
        
        return redirect('/gallery');
    }
    
    public function getYearGallery($year){
        $imagesArray = Imagene::where([
            ['date', '<=', $year.'-12-31'],
            ['date', '>=', $year.'-01-01']
        ])->paginate(3);
        $fechas = getDateSelection();
        $mesesString = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        
        return view('image.gallery', compact('imagesArray', 'fechas', 'mesesString'));
    }
    public function getDayGallery($year, $month, $day){
        $imagesArray = Imagene::where([
            ['date', '=', $year.'-'.$month.'-'.$day]
        ])->paginate(3);
        $fechas = getDateSelection();
        $mesesString = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        
        return view('image.gallery', compact('imagesArray', 'fechas', 'mesesString'));
    }
    /*
    public function getMonthGallery($year,$month){
        if($month<10){
            $month = "0".$month;
        }
        $imagenesArray = Imagene::all();
        $imagesArray = [];
        foreach ($imagesArray as $image){
            if (substr($image->date, 5, 2) == $month){
                array_push($yearImages, $image);
            }
        }
        $imagesArray = array_slice($imagesArray, 0, 3); 
        $fechas = getDateSelection();
        $mesesString = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        
        return view('image.gallery', compact('imagesArray', 'fechas', 'mesesString'));
    }
    */
    public function getMonthGallery($year,$month){

        if($month<10){
            $month = "0".$month;
        }

        if($month == "01" || $month == "03" || $month == "05" || $month == "07" || $month == "08" || $month == "10" || $month == "12"){
            $imagesArray = Imagene::where([
                ['date', '<=', $year.'-'.$month.'-31'],
                ['date', '>=', $year.'-'.$month.'-01']
            ])->paginate(3);
        }else if ($month == "02"){
            $imagesArray = Imagene::where([
                ['date', '<=', $year.'-'.$month.'-28'],
                ['date', '>=', $year.'-'.$month.'-01']
            ])->paginate(3);            
        }else {
            $imagesArray = Imagene::where([
                ['date', '<=', $year.'-'.$month.'-30'],
                ['date', '>=', $year.'-'.$month.'-01']
            ])->paginate(3);
        }

        $fechas = getDateSelection();
        $mesesString = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        
        return view('image.gallery', compact('imagesArray', 'fechas', 'mesesString'));

    }
}