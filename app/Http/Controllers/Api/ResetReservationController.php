<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User_reservation;
use App\Http\Controllers\Reservation\ResetReservationController as noApiReset;

class ResetReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        return json_encode(User_reservation::where('id',$id)->get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function update(Request $request)
    {
        $email = $request->json('mail');
        $token = $request->json('token');
        if(noApiReset::checkResetResponse($token, $email)){
            $userReserve = User_reservation::findOrFail(decrypt($token));
            if(noApiReset::getAssistantsNumber($userReserve->assistants, $userReserve->idReservation) >= $request->json('assistants') && 
                    $request->json('assistants') > 0){
                $userReserve->name = $request->json('name');
                $userReserve->telephone = $request->json('phone');
                $userReserve->assistants = $request->json('assistants');
                $userReserve->observations = $request->json('obs');
                $userReserve->save();
                return 'true';
            }
        }
        return 'false';
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function delete(Request $request){
        $email = $request->json('mail');
        $token = $request->json('token');
        if(noApiReset::checkResetResponse($token, $email)){
            $userReserve = User_reservation::findOrFail(decrypt($token));
            $userReserve->delete();
            
            noApiReset::checkDeleteResetLink($email); // Delete reset link
            return 'true';
        }
        return 'false';
    }
    
    private static function checkData($assistants,$id,$request){
        $noError = true;
        if(!noApiReset::getAssistantsNumber($assistants, $id) >= $request->json('assistants')){
            $noError = false;
        }
        if($request->json('assistants') <=0){
            $noError = true;
        }
        if(!preg_match("/^[0-9]{9}$/", $request->json('phone'))) {
            $noError = false;
        }
        if(filter_var($request->json('email'), FILTER_VALIDATE_EMAIL)){
            
        }   
    }
}