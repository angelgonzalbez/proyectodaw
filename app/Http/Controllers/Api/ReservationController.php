<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User_reservation;
use App\Reservation;

class ReservationController extends Controller {

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
    }
    
    public function get($id){
        return json_encode(User_reservation::where('id', $id)->get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $user_reservation = User_reservation::where('id',$id)->first();
        
        if($user_reservation){
            if(self::checkData($user_reservation->assistants, $user_reservation->idReservation, $request)){
                
                $user_reservation->name = $request->json('name');
                $user_reservation->email = $request->json('email');
                $user_reservation->telephone = $request->json('phone');
                $user_reservation->assistants = $request->json('assistants');
                $user_reservation->observations = $request->json('comments');
                $user_reservation->save();
            
                return 'true';
            }
        }
        return 'false';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function delete($id) {

        $userReserve = User_reservation::findOrFail($id);
        if ($userReserve) {
            $userReserve->delete();
            return 'true';
        }
        return 'false';
    }
    
    public function getMaxAssistants($id){
        $users_reservations = User_reservation::where('idReservation',$id)->get();
        $reservation = Reservation::where('id',$id)->first();
        $max = ($reservation->spaces) + ($reservation->overbooking);
        foreach ($users_reservations as $user_rservation) {
            $max -= $user_rservation->assistants;
        }
        return json_encode($max);
    }
    
    private static function getAssistantsMaxium($assistants, $id){
        $users_reservations = User_reservation::where('idReservation',$id)->get();
        $reservation = Reservation::where('id',$id)->first();
        $max = ($reservation->spaces) + ($reservation->overbooking);
        foreach ($users_reservations as $user_rservation) {
            $max -= $user_rservation->assistants;
        }
        return $max+$assistants;
    }

    private static function checkData($assistants,$id,$request){
        $noError = true;
        
        if(!self::getAssistantsMaxium($assistants, $id) >= $request->json('assistants')){
            $noError = false;
        }
        if($request->json('assistants') <=0){
            $noError = true;
        }
        if(!preg_match("/^[0-9]{9}$/", $request->json('phone'))) {
            $noError = false;
        }
        if(!filter_var($request->json('email'), FILTER_VALIDATE_EMAIL)){
            $noError = false;
        }
        return $noError;
    }
}
