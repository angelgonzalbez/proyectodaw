<?php

namespace App\Http\Controllers;

class ErrorController extends Controller
{
    public function notFound()
    {
        return view('errors.404');
    }
    
    public function fatal()
    {
        return view('errors.500');
    }
    
    public function expired(){
        return view('errors.419');
    }
    
    public function noAutorization(){
        return view('errors.403');
    }
    
    public function dbConnectionError(){
        return view('errors.sql');
    }
}