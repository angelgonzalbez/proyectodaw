<?php


use App\Imagene;
use App\Teacher;


function random_string() {
    $key = '';
    $keys = array_merge(range('a', 'z'), range(0, 9));

    for ($i = 0; $i < 10; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}

function checkActiveMenuOption(string $menuOption): bool {
    
    return $_SERVER['REQUEST_URI'] === $menuOption;
    
}

function date_time_to_string($date, $time) {
    $fecha = getdate(strtotime($date));
    return array_get($fecha, "mday") . "/" . array_get($fecha, "mon") . "/" . array_get($fecha, "year") . " " . $time;
}

function fecha_mayor($date) {
    $actual = date('Y-m-d');
    if ($date >= $actual) {
        return true;
    }
    return false;
}

function getDateSelection() {
    $imagenes = Imagene::all();
    $fechas = array();

    foreach ($imagenes as $imagen) {
        array_push($fechas, $imagen->created_at->year);
    }

    $anyos = array_unique($fechas);
    $arrayAnyos = array();

    foreach ($anyos as $anyo) {
        $arrayAnyos += [$anyo => array()];
    }

    foreach ($imagenes as $imagen) {
        foreach ($arrayAnyos as $anyo => $valor) {
            if ($imagen->created_at->year == $anyo) {
                $arrayAnyos[$anyo] += [$imagen->created_at->month => array()];
            }
        }
    }

    foreach ($imagenes as $imagen) {
        foreach ($arrayAnyos as $anyo => $mesesAnyo) {
            if ($imagen->created_at->year == $anyo) {
                foreach ($mesesAnyo as $mes => $valor) {
                    if ($imagen->created_at->month == $mes) {
                        array_push($arrayAnyos[$anyo][$mes], $imagen->created_at->day);
                    }
                }
            }
        }
    }

    foreach ($arrayAnyos as $anyo => $mesesAnyo) {
        foreach ($mesesAnyo as $mes => $dias) {
            $arrayDias = array_unique($dias);
            $arrayAnyos[$anyo][$mes] = $arrayDias;
        }
    }
    return $arrayAnyos;
}

function getAssistantsNumberOnDate($date) {
    $userReservations = User_reservation::all();
    $assistantsNumber = 0;
    foreach ($userReservations as $userReservation) {
        $dateReservation = Reservation::findOrFail($userReservation->idReservation)->date;
        if ($dateReservation == $date) {
            $assistantsNumber += $userReservation->assistants;
        }
    }
    return $assistantsNumber;
}

function getSpacesLeftOnDate($idReservation) {
    $reservation = Reservation::findOrFail($idReservation);
    $assistants = getAssistantsNumberOnDate($reservation->date);
    $spacesLeft = $reservation->spaces - $reservation->$assistants;
    if ($spacesLeft > 0) {
        return $spacesLeft + $reservation->overbooking;
    }
    return $spacesLeft;
}

function getDepartmentTeachers($department) {
    $teachers = Teacher::all();
    $departmentTeachers = [];
    foreach ($teachers as $teacher) {
        if ($teacher->department == $department) {
            array_push($departmentTeachers, $teacher);
        }
    }
    return $departmentTeachers;
}

/**
function esOpcionMenuActivaEnArray(array $opcionesMenu): bool {

    foreach ($opcionesMenu as $opcionMenu) {

        if (self::esOpcionMenuActiva($opcionMenu) === true) {
            return true;
        }
    }
    return false;
}
 */
