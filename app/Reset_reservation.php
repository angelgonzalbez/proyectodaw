<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reset_reservation extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'email';
}
