<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Reset_reservation;

class DeleteResetUserReservation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:resetreservation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete user reset reservations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reset_reservations = Reset_reservation::where('created_at',date('Y-m-d'))->get();
        foreach ($reset_reservations as $reset) {
            $reset->delete();
        }
    }
}
