<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\MailController;
use App\Http\Controllers\Reservation\ResetReservationController;
use App\Reservation;

class SendUserEmail extends Command
{
    private static $MESSAGE = "Nos ponenmos en contacto para recordarle que tiene una reserva pendiente.";

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:sendusermail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email to the client two days before booking';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $reservations = Reservation::where('date',date('Y-m-d'))
               ->orWhere('date',date('Y-m-d', strtotime(date('Y-m-d').' + 2 days')))
               ->get();

       foreach ($reservations as $reservation) {
           foreach ($reservation->user_reservation as $user ) {
               if($user->email){
                   
                    $link = ResetReservationController::makeResetLink($user);
                    MailController::sendReservationMail
                        ($user->email, $user->name, $reservation->timetable, $reservation->date,$user->assistants, self::$MESSAGE,0,$link);
                }
           }
        }
    }
}