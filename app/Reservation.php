<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'date', 'timetable', 'spaces', 'overbooking', 'waitingList'
    ];
    
    public function user_reservation(){
        return $this->hasMany('App\User_reservation', 'idReservation','id');
    }
}
